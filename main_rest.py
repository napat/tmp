import util_base64url
import util_fileio
from flask import Flask, jsonify, request, redirect, make_response, abort
import faceapi
from modelFaceClassifier import mfc

app = Flask(__name__)

@app.route('/facecmp/v1_0/sameface', methods=['GET', 'POST'])
def sameface():
    iserror = False
    cause = ''

    tmpimg1 = './tmp/tmpfacecmp_img1.jpg'
    tmpimg2 = './tmp/tmpfacecmp_img2s.jpg'
    default_hyper_tolerance = 0.5

    # You can change this to any folder on your system
    ALLOWED_EXTENSIONS = {'png', 'jpg', 'jpeg', 'gif'}

    # input handler
    #print(request.json)
    if not request.json or not 'img1b64url' or not 'img2b64url' in request.json:
        abort(400)

    img1b64url = request.json['img1b64url']
    img2b64url = request.json['img2b64url']

    if not 'img1_extension' in request.json:
        img1_extension = 'jpg'
    else:
        img1_extension = request.json['img1_extension']

    if img1_extension.lower() not in ALLOWED_EXTENSIONS:
        iserror = True
        cause = "Bad file extension"

    if not 'img2_extension' in request.json:
        img2_extension = 'jpg'
    else:
        img2_extension = request.json['img2_extension']

    if img2_extension.lower() not in ALLOWED_EXTENSIONS:
        iserror = True
        cause = "Bad file extension"

    if not 'hyper_tolerance' in request.json:
        hyper_tolerance = default_hyper_tolerance
    else:
        hyper_tolerance = float(request.json['hyper_tolerance'])

    if isinstance(hyper_tolerance, float) != True or 0 > hyper_tolerance or hyper_tolerance > 1:
        iserror = True
        cause = "Bad hyper_tolerance"

    if iserror == True:
       # Return the result as json
        result = {
            "input": {
                "img1b64url": img1b64url,
                "img1_extension": img1_extension,
                "img2b64url": img2b64url,
                "img2_extension": img2_extension,
                "hyper_tolerance": hyper_tolerance,
            },
            "error": {
                "iserror":  iserror,
                "cause":    cause,
            },
        }
        return jsonify(result)

    bimg1b64url = img1b64url.encode()
    bimg1 = util_base64url.base64urldecode(bimg1b64url)
    util_fileio.write_imgbuf(tmpimg1, bimg1)

    bimg2b64url = img2b64url.encode()
    bimg2 = util_base64url.base64urldecode(bimg2b64url)
    util_fileio.write_imgbuf(tmpimg2, bimg2)

    _, is_same_person, img1_facecount, img2_facecount = faceapi.facecmptwoimg(tmpimg1, tmpimg2, hyper_tolerance)
    result = {
        "img1_facecount": img1_facecount,
        "img2_facecount": img2_facecount,
        "is_same_person": is_same_person,
    }
    return jsonify(result)


@app.route('/facecmp/v1_0/faceimg128d', methods=['GET', 'POST'])
def faceimg128d():
    iserror = False
    cause = ''

    tmpimg = './tmp/tmpfaceimg128d.jpg'
    default_hyper_tolerance = 0.5

    # You can change this to any folder on your system
    ALLOWED_EXTENSIONS = {'png', 'jpg', 'jpeg', 'gif'}

    # input handler
    #print(request.json)
    if not request.json or not 'imgb64url' in request.json:
        abort(400)

    imgb64url = request.json['imgb64url']

    if not 'img_extension' in request.json:
        img_extension = 'jpg'
    else:
        img_extension = request.json['img_extension']

    if img_extension.lower() not in ALLOWED_EXTENSIONS:
        iserror = True
        cause = "Bad file extension"

    if not 'hyper_tolerance' in request.json:
        hyper_tolerance = default_hyper_tolerance
    else:
        hyper_tolerance = float(request.json['hyper_tolerance'])

    if isinstance(hyper_tolerance, float) != True or 0 > hyper_tolerance or hyper_tolerance > 1:
        iserror = True
        cause = "Bad hyper_tolerance"

    if iserror == True:
       # Return the result as json
        result = {
            "input": {
                "imgb64url": imgb64url,
                "img_extension": img_extension,
                "hyper_tolerance": hyper_tolerance,
            },
            "error": {
                "iserror":  iserror,
                "cause":    cause,
            },
        }
        return jsonify(result)

    
    bimgb64url = imgb64url.encode()
    bimg = util_base64url.base64urldecode(bimgb64url)
    util_fileio.write_imgbuf(tmpimg, bimg)
    
    #print(tmpimg)
    face128d = faceapi.faceimg128d(tmpimg, hyper_tolerance)

    result = {
        "img_128d": face128d,
    }
    return jsonify(result)


face128d_to_xgb_model = mfc('model/model_lda_xgb.pkl')

@app.route('/facecmp/v1_0/face128d_to_xgb', methods=['GET', 'POST'])
def face128d_to_xgb():
    if not request.json or not 'img_128d' in request.json:
        abort(400)

    img_128d = request.json['img_128d']

    y_class, y_prob = face128d_to_xgb_model.predict_with_prob(img_128d)
    result = {
        "y_class": y_class,
        "y_prob": y_prob,
    }
    return jsonify(result)

face128d_to_knn_model = mfc('model/model_lda_knn.pkl')

@app.route('/facecmp/v1_0/face128d_to_knn', methods=['GET', 'POST'])
def face128d_to_knn():
    if not request.json or not 'img_128d' in request.json:
        abort(400)

    img_128d = request.json['img_128d']

    y_class, y_prob = face128d_to_knn_model.predict_with_prob(img_128d)
    result = {
        "y_class": y_class,
        "y_prob": y_prob,
    }
    return jsonify(result)

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5000, debug=True)
