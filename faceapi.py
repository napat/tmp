import face_recognition
from flask import jsonify

def facerecog(imgpath1, imgpath2, hyper_tolerance=0.5, num_jitters=1):
    p001 = face_recognition.load_image_file(imgpath1)
    p001_encoding = face_recognition.face_encodings(p001, num_jitters=num_jitters)[0]

    p002 = face_recognition.load_image_file(imgpath2)
    p002_encoding = face_recognition.face_encodings(p002, num_jitters=num_jitters)[0]

    results = face_recognition.compare_faces(
        [p001_encoding], p002_encoding, hyper_tolerance)

    issameperson = True if results[0] else False

    #print(type(issameperson))
    return issameperson


def faceimg128d(img1_path, hyper_tolerance=0.5, num_jitters=1):
    img1 = face_recognition.load_image_file(img1_path)
    face1_encodings = face_recognition.face_encodings(
        img1, num_jitters=num_jitters)

    #print(type(face1_encodings[0]))
    #print(len(face1_encodings[0]))
    if len(face1_encodings) <= 0:
        return []
    else:
        return list(face1_encodings[0])


def facecmptwoimg(img1_path, img2_path, hyper_tolerance=0.5, num_jitters=1):
    is_err = True
    img1_facecount = 0
    img2_facecount = 0
    is_same_person = False
    
    img1 = face_recognition.load_image_file(img1_path)
    face1_encodings = face_recognition.face_encodings(img1, num_jitters=num_jitters)

    img2 = face_recognition.load_image_file(img2_path)
    face2_encodings = face_recognition.face_encodings(img2, num_jitters=num_jitters)

    img1_facecount = len(face1_encodings)
    img2_facecount = len(face2_encodings)

    #print(face1_encodings[0])
    #print(face2_encodings[0])
    #print(hyper_tolerance)

    if (img1_facecount <= 0) or (img2_facecount <= 0):

        #result = {
        #    "img1_facecount": img1_facecount,
        #    "img2_facecount": img2_facecount,
        #    "is_same_person": False,
        #}
        #return jsonify(result)        
        return is_err, is_same_person, img1_facecount, img2_facecount

    # See if the first face image matches the second face
    match_results = face_recognition.compare_faces(
        [face1_encodings[0]], face2_encodings[0], hyper_tolerance)

    if match_results[0]:
        is_same_person = True

    # Return the result as json
    is_err = False
    #result = {
    #    "img1_facecount": img1_facecount,
    #    "img2_facecount": img2_facecount,
    #    "is_same_person": is_same_person,
    #}
    #return jsonify(result)
    return is_err, is_same_person, img1_facecount, img2_facecount

'''
def facerecog_process(num_jitters=1):
    outstr = ''

    p001 = face_recognition.load_image_file("cherprang001.jpg")
    p001_encoding = face_recognition.face_encodings(p001, num_jitters=num_jitters)[0]
    p002 = face_recognition.load_image_file("cherprang002.jpg")
    p002_encoding = face_recognition.face_encodings(p002, num_jitters=num_jitters)[0]

    p003 = face_recognition.load_image_file("pun001.jpg")
    p003_encoding = face_recognition.face_encodings(p003, num_jitters=num_jitters)[0]
    p004 = face_recognition.load_image_file("pun002.jpg")
    p004_encoding = face_recognition.face_encodings(p004, num_jitters=num_jitters)[0]
    p005 = face_recognition.load_image_file("pun003.jpg")
    p005_encoding = face_recognition.face_encodings(p005, num_jitters=num_jitters)[0]

    print("-------------")

    results = face_recognition.compare_faces(
        [p001_encoding], p002_encoding, 0.5)
    curstr = f"cherprang001 vs cherprang002: {results[0]}"
    print(curstr)
    outstr += curstr

    results = face_recognition.compare_faces(
        [p003_encoding], p004_encoding, 0.5)
    curstr = f"pun001 vs pun002: {results[0]}"
    print(curstr)
    outstr += "\n<br>"
    outstr += curstr

    results = face_recognition.compare_faces(
        [p003_encoding], p005_encoding, 0.5)
    curstr = f"pun001 vs pun003: {results[0]}"
    print(curstr)
    outstr += "\n<br>"
    outstr += curstr

    results = face_recognition.compare_faces(
        [p004_encoding], p005_encoding, 0.5)
    curstr = f"pun002 vs pun003: {results[0]}"
    print(curstr)
    outstr += "\n<br>"
    outstr += curstr

    print("-------------")
    outstr += "\n<br>"
    outstr += curstr

    results = face_recognition.compare_faces(
        [p001_encoding], p003_encoding, 0.5)
    curstr = f"cherprang001 vs pun001: {results[0]}"
    print(curstr)
    outstr += "\n<br>"
    outstr += curstr

    results = face_recognition.compare_faces(
        [p001_encoding], p004_encoding, 0.5)
    curstr = f"cherprang001 vs pun002: {results[0]}"
    print(curstr)
    outstr += "\n<br>"
    outstr += curstr

    results = face_recognition.compare_faces(
        [p001_encoding], p005_encoding, 0.5)
    curstr = f"cherprang001 vs pun003: {results[0]}"
    print(curstr)
    outstr += "\n<br>"
    outstr += curstr

    results = face_recognition.compare_faces(
        [p002_encoding], p003_encoding, 0.5)
    curstr = f"cherprang002 vs pun001: {results[0]}"
    print(curstr)
    outstr += "\n<br>"
    outstr += curstr

    results = face_recognition.compare_faces(
        [p002_encoding], p004_encoding, 0.5)
    curstr = f"cherprang002 vs pun002: {results[0]}"
    print(curstr)
    outstr += "\n<br>"
    outstr += curstr

    results = face_recognition.compare_faces(
        [p002_encoding], p005_encoding, 0.5)
    curstr = f"cherprang002 vs pun003: {results[0]}"
    print(curstr)
    outstr += "\n<br>"
    outstr += curstr

    return outstr
'''
